<?php
session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

?>

<html lang="en">
<head>
    <!------------------------ Required meta tags --------------------------------------->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!------------------------ Bootstrap CSS -------------------------------------------->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>

    <!------------------------ GOOGLE FONTS --------------------------------------------------->
    <link href="https://fonts.googleapis.com/css?family=Lato:300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@700&display=swap" rel="stylesheet">

    <!---------------------- BootStrap Offline ------------------------------->
	<link href="./bootstrap-5.1.3-dist/css/bootstrap.min.css" rel="stylesheet">

    <!---------------------- FontAwesome --------------------------->
    <script src="https://kit.fontawesome.com/b66bb9ffd2.js"></script>
        
    <!----------------------PERSONAL CSS ------------------------>
    <link rel="stylesheet" type="text/css" href="./front-end/css/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="./front-end/css/posting.css">

    <!--------------------- TITLE AREA -------------->
    <link rel="icon" href="./front-end/img/cic-logo.png">
    <title>CIC Free Dikit</title>

    <!------------------ HEADER TOP MENU BAR (Black) -------------------->
	<div class = "container-fluid head-background">
		<div class = "container">
			<div class="navbar navbar-expand-lg navbar-dark top-nav-bar">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
					<div class = "mr-auto">
						<div class="navbar-nav">
							<img src="./front-end/img/icons8-commercial-90.png" class="announcement-icon"/>
							<a class="nav-item nav-link header-buttons" href="announcement.html">Announcement</a>
							<img src="./front-end/img/icons8-event-64.png" class="events-icon">
							<a class="nav-item nav-link header-buttons" href="C:\xampp\htdocs\cic_free_dikit-1\Events.html">Events</a>
							<img  src="./front-end/img/icons8-news-480.png" class="colleges-news-icon">
							<a class="nav-item nav-link header-buttons" href="#">Colleges News</a>
						</div>
					</div>
                <form method="post" action="db/server.php">
                    <div class = "ml-auto">
						<div class="navbar-nav">
             				 <img src="./front-end/img/icons8-user-90.png" class="profile-icon">
							<a class="nav-item nav-link header-buttons profilebutton" id="dropdownMenuButton" data-toggle="dropdown" href="profile.php">Profile</a>
							<a class="nav-item nav-link header-buttons" href="#">|
                                <form>
                                <input type="submit" class="btn btn-outline-warning" style="margin-left: 10px; margin-top:-7px;" value="logout" name="logout">
								</form>
							<!-- <span class="border-left border-white header-separator header-buttons" href="login.html"></span>Log Out</a> -->
						</div>
					</div>
                </form>
				
				</div>
			</div>
		</div>
	</div> 
<body>
	<!-- For Newsfeed Post Area -->
	<div class="container newsfeedcontainer">
		<div class="profile-wrapper">
		   
			<!-- /.profile-section-user -->
			<div class="profile-section-main">
				<!-- Tab panes -->
				<div class="tab-content profile-tabs-content">
					<div class="tab-pane active" id="profile-overview" role="tabpanel">
						<div class="post-editor">
							<form method="post" action="db/server.php">
								<textarea name="posts" class="post-field" placeholder="Wazzup ka Free Dikit?"></textarea>
								<div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="post_type" id="inlineRadio1" value="Post">
  											<label class="form-check-label" for="inlineRadio1" href="C:\xampp\htdocs\cic_free_dikit-1\announcement.html">Announcement Post</label>
									 </div>
									 <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="post_type" id="inlineRadio1" value="Event">
  											<label class="form-check-label" for="inlineRadio1">Event Post</label>
									 </div>
									 <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="post_type" id="inlineRadio1" value="Announcement">
  											<label class="form-check-label" for="inlineRadio1">Colleges News Post</label>
									 </div><br>    
									 <input type="file" class="form-control" name="post_file" style="padding-top:15px; padding-bottom:40px;"/><br>      
								<div class="d-flex">
									                                      	
                                  
									<button class="btn btn-success px-5 py-2" type="submit" name="add">Post</button>

									<!-- <input type="file" src="./img/file-64px.png" class="file-icon sp-author-name"> -->
									<!-- <img src="./img/file-64px.png" class="file-icon sp-author-name"> -->
									<!-- <img src="./img/image-96px.png" class="image-icon"> -->
								</div>	
							</form>				
							<!-- <button class="btn btn-success px-4 py-1">Add Source</button>				 -->
						</div>

						<div class="container-fluid qr-container">
							<div class="text-center">
							<img src="https://chart.googleapis.com/chart?cht=qr&chl=Hello+World&chs=160x160&chld=L|0" class="qr-code img-thumbnail img-responsive" style="margin-top: 25px; border-color: maroon;" />
							</div>
						
							<div class="form-horizontal">
									<div class="form-group">
										<label class="control-label col-sm-3"for="content" style="margin-top: 25px;">Link:</label>
										<div class="col-sm-6">
								<!-- Input box to enter the required data -->
											<input type="text" style="margin-top: 25px;" size="60" maxlength="60" class="form-control"id="content" placeholder="Enter Reference Link"/>
										</div>
									</div>
								
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<!-- Button to generate QR Code for the entered data -->
										<button type="button" class="btn btn-primary" id="generate" style="margin-left:65px;">Generate</button>
									</div>
								</div>
							</div>
						</div>
						<!-- post-newsfeed -->
						<div class="stream-posts">
							<div class="stream-post">
								<div class="sp-author">
									<a href="#" class="sp-author-avatar"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt=""></a>
									<h6 class="sp-author-name"><a href="#">John Doe</a></h6></div>
								<div class="sp-content">
									<div class="sp-info">posted 1h ago</div>
									<p class="sp-paragraph mb-0">Auk Soldanella plainscraft acetonylidene wolfishness irrecognizant Candolleaceae provision Marsipobranchii arpen Paleoanthropus supersecular inidoneous autoplagiarism palmcrist occamy equestrianism periodontoclasia mucedin overchannel goelism decapsulation pourer zira</p>
								</div>
								<!-- /.sp-content -->
							</div>
							<!-- /.stream-post -->
							<div class="stream-post">
								<div class="sp-author">
									<a href="#" class="sp-author-avatar"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt=""></a>
									<h6 class="sp-author-name"><a href="profile.html">Palmira Guthridge</a></h6></div>
								<div class="sp-content">
									<div class="sp-info">posted 1h ago</div>
									<p class="sp-paragraph mb-0">Auk Soldanella plainscraft acetonylidene wolfishness irrecognizant Candolleaceae provision Marsipobranchii arpen Paleoanthropus supersecular inidoneous autoplagiarism palmcrist occamy equestrianism periodontoclasia mucedin overchannel goelism decapsulation pourer zira</p>
								</div>
								<!-- /.sp-content -->
							</div>
							<!-- /.stream-post -->
							<div class="stream-post">
								<div class="sp-author">
									<a href="#" class="sp-author-avatar"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt=""></a>
									<h6 class="sp-author-name"><a href="profile.html">Meghann Fraser</a></h6></div>
								<div class="sp-content">
									<div class="sp-info">posted 2h ago</div>
									<p class="sp-paragraph mb-0">Auk Soldanella plainscraft acetonylidene wolfishness irrecognizant Candolleaceae provision Marsipobranchii arpen Paleoanthropus supersecular inidoneous autoplagiarism palmcrist occamy equestrianism periodontoclasia mucedin overchannel goelism decapsulation pourer zira</p>
								</div>
								<!-- /.sp-content -->
							</div>
							<!-- /.stream-post -->
							<div class="stream-post">
								<div class="sp-author">
									<a href="profile.html" class="sp-author-avatar"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt=""></a>
									<h6 class="sp-author-name"><a href="profile.html">Kent Lemaitre</a></h6></div>
							 <div class="sp-content">
									<div class="sp-info">posted 1h ago</div>
									<p class="sp-paragraph mb-0">Auk Soldanella plainscraft acetonylidene wolfishness irrecognizant Candolleaceae provision Marsipobranchii arpen Paleoanthropus supersecular inidoneous autoplagiarism palmcrist occamy equestrianism periodontoclasia mucedin overchannel goelism decapsulation pourer zira</p>
								</div>
								<!-- /.sp-content -->
							</div>
							<!-- /.stream-post -->
							<div class="stream-post mb-0">
								<div class="sp-author">
									<a href="profile.html" class="sp-author-avatar"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" alt=""></a>
									<h6 class="sp-author-name"><a href="#">Loria Lambing</a></h6></div>
								<div class="sp-content">
									<div class="sp-info">posted 2 days ago</div>
									<p class="sp-paragraph">Auk Soldanella plainscraft acetonylidene wolfishness irrecognizant Candolleaceae provision Marsipobranchii arpen Paleoanthropus supersecular inidoneous</p>
									<p class="sp-paragraph">autoplagiarism palmcrist occamy equestrianism periodontoclasia mucedin overchannel goelism decapsulation pourer zira</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- Scripts Area -->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
						
	<script>
		// Function to HTML encode the text
		// This creates a new hidden element,
		// inserts the given text into it
		// and outputs it out as HTML
		function htmlEncode(value) {
		return $('<div/>').text(value)
			.html();
		}
	
		$(function () {
	
		// Specify an onclick function
		// for the generate button
		$('#generate').click(function () {
	
			// Generate the link that would be
			// used to generate the QR Code
			// with the given data
			let finalURL =
	'https://chart.googleapis.com/chart?cht=qr&chl=' +
			htmlEncode($('#content').val()) +
			'&chs=160x160&chld=L|0'
	
			// Replace the src of the image with
			// the QR code image
			$('.qr-code').attr('src', finalURL);
		});
		});
	</script>



  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>


    <!-- Bootstrap Javascript Offline -->

     <script href="./bootstrap-4.3.1-dist/js/bootstrap.bundle.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <script href="./bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <script href="./bootstrap-4.3.1-dist/js/bootstrap.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
     <script href="./bootstrap-4.3.1-dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   	



		</body>
</html>
