<?php
// Initialize the session
session_start(); 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index.php");
    exit;
}

?>
 
<html>
    <head>
        <Title>CIC Login</Title>
        <link rel="stylesheet" href="./front-end/css/login.css"/>
		<link rel="icon" href="./front-end/img/cic-logo.png">
    </head>
    <body  style="background-image: url(./front-end/img/eagle.jpg); overflow-x: hidden; overflow-y:auto;">
      
    <div class="login-wrap fullscreen-bg">
        <div class="login-html">
           <div class="group-image">
            <img src="./front-end/img/cic-logo.png" class="icons">
            <img src="./front-end/img/lc-logo.png" class="lc-icon">
           </div>
          <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
          <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>
          <div class="login-form">
			  <form method="post" action="db/server.php">
				<div class="sign-in-htm">
					<div class="group">
					  <label for="user" class="label" placeholder="">ID No</label>
					  <input id="user" type="text" class="input" name="id_no">
					</div>
					<div class="group">
					  <label for="pass" class="label">Password</label>
					  <input id="pass" type="password" class="input" data-type="password" name="password">
					</div>
					<!-- <div class="group">
					  <input id="check" type="checkbox" class="check" checked>
					  <label for="check"><span class="icon"></span> Keep me Signed in</label>
					</div> -->
					<div class="group">
					  <input type="submit" class="button" value="Sign In" name="login">
					</div>              
					  <div class="hr"></div>
				  </div>				
			  </form>
	          <form method="post" action="db/server.php">
				<div class="sign-up-htm">
					<div class="group">
					  <label for="user" class="label">ID No.</label>
					  <input id="user" type="text" class="input" name="id_no">
					</div>
					<div class="group">
					  <label for="user" class="label">First Name</label>
					  <input id="text" type="text" class="input"  name="fname">
					</div>
					<div class="group">
					  <label for="pass" class="label">Last Name</label>
					  <input id="pass" type="text" class="input" name="lname">
					</div>
					<div class="group">
						<label for="email" class="label">Email</label>
						<input id="email" type="email" class="input" name="email">
					  </div>
					<div class="group">
					  <label for="pass" class="label">Password</label>
					  <input id="pass" type="password" class="input" name="password">
					</div>  
	  
	  
					<div class="group">
					  <input type="submit" class="button" value="Register" name="register">
					</div>
				  </div>
			  </form>
          </div>
        </div>
      </div>
    </body>
</html>
