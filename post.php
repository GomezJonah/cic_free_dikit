<!DOCTYPE html>
<!-- Coding By CodingNepal - youtube.com/codingnepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Free Dikit Post Page</title>
    <link rel="stylesheet" href="./front-end/css/post.css ">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- FontAweome CDN Link for Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
  </head>
  <body>
    <div class="container">
      <div class="wrapper">
        <section class="post">
          <header>Create Post</header>
          <form action="#">
            <div class="content">
              <!-- <img src="ic.png" alt="logo"> -->
        
            </div>
            <textarea placeholder="What's on your mind, Free Dikit?" spellcheck="false" required></textarea>
             <!-- <div class="theme-emoji">
              <img src="icons/theme.svg" alt="theme">
              <img src="icons/smile.svg" alt="smile">
            </div> -->
            <div class="options">
              <p>Add to Your Post</p>
              <ul class="list add-file">
                <li><img src="./img/image-96px.png" href="#"></li>
                <li><img src="./img/people-96px.png" href="#"></li>
                <li><img src="./img/file-64px.png" href="#"></li>
                <!-- <li><img src="icons/mic.svg"   href="file"></li>
                <li><img src="icons/more.svg"  href="file"></li>  -->
              </ul>
            </div>
            <button>Dikit</button>
          </form> 
        </section>

      </div>
    </div>

    <script>
      const container = document.querySelector(".container"),
      privacy = container.querySelector(".post .privacy"),
      arrowBack = container.querySelector(".audience .arrow-back");

      privacy.addEventListener("click", () => {
        container.classList.add("active");
      });

      arrowBack.addEventListener("click", () => {
        container.classList.remove("active");
      });
    </script>
    
  </body>
</html>